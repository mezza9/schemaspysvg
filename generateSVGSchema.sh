#!/bin/bash

OPTS=`getopt -o hu:d:p: --long help,user:,database:,password: -n 'parse-options' -- "$@"`
PG_DRIVER=postgresql-9.4.1208.jar
SCHEMA_SPY_JAR=schemaSpy_5.0.0.jar

usage() 
{
  echo "Usage: $0 -u <USERNAME FOR DB> -d <DB NAME> -f <PATH FOR DUMP> [-I <\"(TABLE TO EXCLUDE)|(TABLE TO EXCLUDE)\">] [-p <PASSWORD FOR DB>]"
  exit 0
}

while getopts "hu:p:d:f:I:" flag
do
  case $flag in
    u) u=$OPTARG;;
    p) p=$OPTARG;;
    d) db=$OPTARG;;
    f) output=$OPTARG;;
    I) I=$OPTARG;;
    *) usage;;
  esac
done

if [ -z "$u" ] || [ -z "$db" ] || [ -z "$output" ]
  then
    usage
fi

# Generate schema diagrams
java -jar $SCHEMA_SPY_JAR -t pgsql -u $u -db $db -I $I -host localhost -o $output -s public -dp $PG_DRIVER

# Convert all PNGs to DOTs
dot -O -Tsvg $output/diagrams/*.dot
dot -O -Tsvg $output/diagrams/summary/*.dot

# Remove all PNGs and DOTs
rm $output/diagrams/*.png
rm $output/diagrams/*.dot
rm $output/diagrams/summary/*.png
rm $output/diagrams/summary/*.dot

# Replace all references in HTMLs to .PNG to .DOT.SVG
sed -i '.backup' -e "s/.png/.dot.svg/g" $output/*.html
sed -i '.backup' -e "s/.png/.dot.svg/g" $output/tables/*.html

# Remove all backup files
rm $output/*.backup
rm $output/tables/*.backup